package jedyny.android.doisttestproject.data

import androidx.room.Room
import com.squareup.moshi.Moshi
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module

val databaseModule = module {
    single {
        val database = Room.databaseBuilder(androidContext(), Database::class.java, "database")
            .fallbackToDestructiveMigration()
            .build()

        database.dao()
    }

    single { Moshi.Builder().build() }
}