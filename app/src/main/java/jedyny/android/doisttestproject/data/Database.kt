package jedyny.android.doisttestproject.data

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [AttachmentEntity::class, MessageEntity::class, UserEntity::class], version = 1)
abstract class Database : RoomDatabase() {
    abstract fun dao(): MessagesDao
}

