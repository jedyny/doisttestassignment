package jedyny.android.doisttestproject.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Data(
    val messages: List<Message>,
    val users: List<User>
)

@JsonClass(generateAdapter = true)
data class Message(
    val id: Long,
    val userId: Long,
    val content: String,
    val attachments: List<Attachment> = listOf()
)

@JsonClass(generateAdapter = true)
data class User(
    val id: Long,
    val name: String,
    val avatarId: String
)

@JsonClass(generateAdapter = true)
data class Attachment(
    val id: String,
    val title: String,
    val url: String,
    val thumbnailUrl: String
)