package jedyny.android.doisttestproject.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import jedyny.android.doisttestproject.R
import jedyny.android.doisttestproject.util.observeNonNulls
import jedyny.android.doisttestproject.util.observeNotHandled
import kotlinx.android.synthetic.main.messages_screen.*
import org.koin.android.viewmodel.ext.android.viewModel

class MessagesActivity : AppCompatActivity(),
        MessageAdapter.Callbacks,
        DeleteMessageDialogFragment.Callbacks,
        DeleteAttachmentDialogFragment.Callbacks
{
    private val viewModel by viewModel<MessagesViewModel>()

    private val adapter = MessageAdapter(this)
    private var canRequestNextPage = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.messages_screen)

        initMessageRecycler()

        viewModel.adapterItems.observeNonNulls(this) { onNewAdapterItems(it) }
        viewModel.showDeleteMessageDialog.observeNotHandled(this) { showDeleteMessageDialog(it) }
        viewModel.showDeleteAttachmentDialog.observeNotHandled(this) { showDeleteAttachmentDialog(it) }
    }

    private fun initMessageRecycler() {
        val layoutManager = LinearLayoutManager(this)

        message_recycler.layoutManager = layoutManager
        message_recycler.adapter = adapter

        message_recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (canRequestNextPage && dy > 0) { // Scrolling down
                    val visibleItemCount = layoutManager.childCount
                    val totalItemCount = layoutManager.itemCount
                    val pastInvisibleItemCount = layoutManager.findFirstVisibleItemPosition()

                    if (totalItemCount - pastInvisibleItemCount <= visibleItemCount) {
                        requestNextPage()
                    }
                }
            }
        })
    }

    private fun onNewAdapterItems(adapterItems: AdapterItems) {
        adapter.submitList(adapterItems.adapterItems)

        canRequestNextPage = adapterItems.hasMore
    }

    private fun requestNextPage() {
        canRequestNextPage = false
        viewModel.loadNextPage()
    }

    override fun onMyMessageLongClicked(adapterItem: MyMessage) {
        viewModel.onMyMessageLongClicked(adapterItem)
    }

    override fun onOtherMessageLongClicked(adapterItem: OtherMessage) {
        viewModel.onOtherMessageLongClicked(adapterItem)
    }

    private fun showDeleteMessageDialog(event: ShowDeleteMessageDialogEvent) {
        DeleteMessageDialogFragment.show(this, event.messageId)
    }

    override fun onDeleteMessageClicked(messageId: Long) {
        viewModel.deleteMessage(messageId)
    }

    override fun onMyAttachmentLongClicked(adapterItem: MyAttachment) {
        viewModel.onMyAttachmentLongClicked(adapterItem)
    }

    override fun onOtherAttachmentLongClicked(adapterItem: OtherAttachment) {
        viewModel.onOtherAttachmentLongClicked(adapterItem)
    }

    private fun showDeleteAttachmentDialog(event: ShowDeleteAttachmentDialogEvent) {
        DeleteAttachmentDialogFragment.show(this, event.attachmentId)
    }

    override fun onDeleteAttachmentClicked(attachmentId: Long) {
        viewModel.deleteAttachment(attachmentId)
    }
}




