package jedyny.android.doisttestproject.data

import android.content.SharedPreferences
import android.content.res.Resources
import com.squareup.moshi.Moshi
import io.reactivex.Single
import jedyny.android.doisttestproject.R
import jedyny.android.doisttestproject.SchedulerProvider
import okio.Okio

class DatabasePopulator(
    private val prefs: SharedPreferences,
    private val messagesDao: MessagesDao,
    private val moshi: Moshi,
    private val resources: Resources,
    private val schedulerProvider: SchedulerProvider
) {
    companion object {
        private const val IS_POPULATED_PREF = "isDatabasePopulated"
    }

    fun populateDatabaseIfNeeded() {
        val isPopulated = prefs.getBoolean(IS_POPULATED_PREF, false)

        if (!isPopulated) {
            getInitialData()
                .subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.computation)
                .subscribe(this::onInitialDataLoaded)
        }
    }

    private fun getInitialData(): Single<Data> {
        return Single.fromCallable {
            Okio.buffer(Okio.source(resources.openRawResource(R.raw.data))).use {
                moshi.adapter(Data::class.java).fromJson(it)
            }
        }
    }

    private fun onInitialDataLoaded(data: Data) {
        val messageEntities = mutableListOf<MessageEntity>()
        val attachmentEntities = mutableListOf<AttachmentEntity>()
        val userEntities = mutableListOf<UserEntity>()

        for (message in data.messages) {
            messageEntities += message.toEntity()
            attachmentEntities += message.attachments.map { it.toEntity(message.id) }
        }

        userEntities += data.users.map { it.toEntity() }

        messagesDao.insertAll(messageEntities, attachmentEntities, userEntities)

        prefs.edit()
            .putBoolean(IS_POPULATED_PREF, true)
            .apply()
    }

    private fun Message.toEntity() = MessageEntity(
        id = id,
        userId = userId,
        content = content)

    private fun User.toEntity() = UserEntity(
        id = id,
        name = name,
        avatarId = avatarId
    )

    private fun Attachment.toEntity(messageId: Long) = AttachmentEntity(
        id = 0,
        uuid = id,
        messageId = messageId,
        title = title,
        url = url,
        thumbnailUrl = thumbnailUrl
    )
}