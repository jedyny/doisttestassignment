package jedyny.android.doisttestproject.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import io.reactivex.Flowable

@Dao
interface MessagesDao {
    @Insert
    fun insertAll(
            messages: Iterable<MessageEntity>,
            attachments: Iterable<AttachmentEntity>,
            users: Iterable<UserEntity>)

    @Query("DELETE FROM message WHERE id = :messageId")
    fun deleteMessage(messageId: Long)

    @Query("DELETE FROM attachment WHERE id = :attachmentId")
    fun deleteAttachment(attachmentId: Long)

    @Query(
        """
            SELECT
                message.id AS message_id,
                message.userId AS message_userId,
                message.content AS message_content,
                user.id AS author_id,
                user.name AS author_name,
                user.avatarId AS author_avatarId
            FROM message INNER JOIN user
            ON message.userId = user.id
            LIMIT :limit
        """)
    fun getMessages(limit: Int): Flowable<List<MessageWithAuthorAndAttachments>>
}