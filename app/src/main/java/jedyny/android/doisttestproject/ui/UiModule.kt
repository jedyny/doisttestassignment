package jedyny.android.doisttestproject.ui

import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val uiModule = module {
    viewModel { MessagesViewModel(get(), get()) }
}