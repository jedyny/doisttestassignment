package jedyny.android.doisttestproject.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import jedyny.android.doisttestproject.R
import jedyny.android.doisttestproject.util.RoundedImageTarget
import kotlinx.android.synthetic.main.messages_my_attachment_item.view.*
import kotlinx.android.synthetic.main.messages_my_message_item.view.*
import kotlinx.android.synthetic.main.messages_other_attachment_item.view.*
import kotlinx.android.synthetic.main.messages_other_message_item.view.*

class MessageAdapter(private val callbacks: Callbacks) : ListAdapter<MessageAdapterItem, RecyclerView.ViewHolder>(
    MessageDiffCallback
) {
    companion object {
        const val ITEM_TYPE_MESSAGE = 0x1000000000000000
        const val ITEM_TYPE_ATTACHMENT = 0x2000000000000000

        const val VIEW_TYPE_MY_MESSAGE = R.layout.messages_my_message_item
        const val VIEW_TYPE_OTHER_MESSAGE = R.layout.messages_other_message_item
        const val VIEW_TYPE_MY_ATTACHMENT = R.layout.messages_my_attachment_item
        const val VIEW_TYPE_OTHER_ATTACHMENT = R.layout.messages_other_attachment_item
    }

    init {
        setHasStableIds(true)
    }

    override fun getItemId(position: Int): Long {
        val item = getItem(position)

        /*
         * We reserve 4 most significant bits to store the type information. Since ids are positive
         * longs incremented from 0, we expect them to never use that bits.
         */
        return when (item) {
            is MyMessage -> ITEM_TYPE_MESSAGE or item.id
            is OtherMessage -> ITEM_TYPE_MESSAGE or item.id
            is MyAttachment -> ITEM_TYPE_ATTACHMENT or item.id
            is OtherAttachment -> ITEM_TYPE_ATTACHMENT or item.id
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is MyMessage -> VIEW_TYPE_MY_MESSAGE
            is OtherMessage -> VIEW_TYPE_OTHER_MESSAGE
            is MyAttachment -> VIEW_TYPE_MY_ATTACHMENT
            is OtherAttachment -> VIEW_TYPE_OTHER_ATTACHMENT
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)

        return when (viewType) {
            VIEW_TYPE_MY_MESSAGE -> MyMessageViewHolder(view)
            VIEW_TYPE_OTHER_MESSAGE -> OtherMessageViewHolder(view)
            VIEW_TYPE_MY_ATTACHMENT -> MyAttachmentViewHolder(view)
            VIEW_TYPE_OTHER_ATTACHMENT -> OtherAttachmentViewHolder(view)
            else -> throw AssertionError("Unknown viewType type $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)

        when (item) {
            is MyMessage -> (holder as MyMessageViewHolder).bind(item)
            is OtherMessage -> (holder as OtherMessageViewHolder).bind(item)
            is MyAttachment -> (holder as MyAttachmentViewHolder).bind(item)
            is OtherAttachment -> (holder as OtherAttachmentViewHolder).bind(item)
        }
    }

    inner class MyMessageViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        private var item: MyMessage? = null

        init {
            itemView.setOnLongClickListener {
                item?.let { callbacks.onMyMessageLongClicked(it) }
                true
            }
        }

        fun bind(message : MyMessage) {
            item = message

            itemView.my_message_content_text.text = message.content
        }
    }

    inner class OtherMessageViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        private val roundedImageTarget = RoundedImageTarget(
            itemView.resources,
            itemView.other_message_author_avatar_image
        )

        private var item: OtherMessage? = null

        init {
            itemView.setOnLongClickListener {
                item?.let { callbacks.onOtherMessageLongClicked(it) }
                true
            }
        }

        fun bind(message : OtherMessage) {
            item = message

            itemView.other_message_author_name_text.text = message.authorName
            itemView.other_message_content_text.text = message.content

            Picasso.get()
                .load(message.authorAvatarUrl)
                .into(roundedImageTarget)
        }
    }

    inner class MyAttachmentViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        private var item: MyAttachment? = null

        init {
            itemView.setOnLongClickListener {
                item?.let { callbacks.onMyAttachmentLongClicked(it) }
                true
            }
        }

        fun bind(attachment: MyAttachment) {
            item = attachment

            itemView.my_attachment_thumbnail_title.text = attachment.title

            Picasso.get()
                    .load(attachment.url)
                    .into(itemView.my_attachment_thumbnail_image)
        }
    }

    inner class OtherAttachmentViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        private var item: OtherAttachment? = null

        init {
            itemView.setOnLongClickListener {
                item?.let { callbacks.onOtherAttachmentLongClicked(it) }
                true
            }
        }

        fun bind(attachment: OtherAttachment) {
            item = attachment

            itemView.other_attachment_thumbnail_title.text = attachment.title

            Picasso.get()
                    .load(attachment.url)
                    .into(itemView.other_attachment_thumbnail_image)
        }
    }

    interface Callbacks {
        fun onMyMessageLongClicked(adapterItem: MyMessage)
        fun onOtherMessageLongClicked(adapterItem: OtherMessage)
        fun onMyAttachmentLongClicked(adapterItem: MyAttachment)
        fun onOtherAttachmentLongClicked(adapterItem: OtherAttachment)
    }
}

private object MessageDiffCallback : DiffUtil.ItemCallback<MessageAdapterItem>() {
    override fun areItemsTheSame(oldItem: MessageAdapterItem, newItem: MessageAdapterItem) = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: MessageAdapterItem, newItem: MessageAdapterItem) = oldItem == newItem
}