package jedyny.android.doisttestproject.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.google.common.truth.Truth.assertThat
import jedyny.android.doisttestproject.TestSchedulerProvider
import jedyny.android.doisttestproject.data.*
import jedyny.android.doisttestproject.testObserver
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MessagesViewModelTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    lateinit var database: Database
    lateinit var messagesDao: MessagesDao

    @Before
    fun initDatabase() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            Database::class.java
        ).build()

        messagesDao = database.dao()
    }

    @After
    fun closeDatabase() {
        database.close()
    }

    @Test fun when_no_messages_emits_empty_adapter_items() {
        val viewModel = MessagesViewModel(messagesDao, TestSchedulerProvider())
        val adapterItemObserver = viewModel.adapterItems.testObserver()

        assertThat(adapterItemObserver.observedValues).containsExactly(
            AdapterItems(listOf(), hasMore = false)
        )
    }

    @Test fun emits_my_message_without_attachment() {
        givenDatabase(
            messages = listOf(message(1L, me().id, "To be or not to be")),
            users = listOf(me())
        )

        val viewModel = MessagesViewModel(messagesDao, TestSchedulerProvider())
        val adapterItemObserver = viewModel.adapterItems.testObserver()

        assertThat(adapterItemObserver.observedValues).containsExactly(
            AdapterItems(listOf(
                MyMessage(1L, "To be or not to be")
            ), hasMore = false)
        )
    }

    @Test fun emits_my_message_with_attachment() {
        givenDatabase(
            messages = listOf(message(1L, me().id, "To be or not to be")),
            attachments = listOf(attachment(1000L, 1L, "hamlet.jpg", "https://fake.com/hamlet")),
            users = listOf(me())
        )

        val viewModel = MessagesViewModel(messagesDao, TestSchedulerProvider())
        val adapterItemObserver = viewModel.adapterItems.testObserver()

        assertThat(adapterItemObserver.observedValues).containsExactly(
            AdapterItems(listOf(
                MyMessage(1L, "To be or not to be"),
                MyAttachment(1000L, "1000", "hamlet.jpg", "https://fake.com/hamlet")
            ), hasMore = false)
        )
    }

    @Test fun emits_other_message_without_attachment() {
        givenDatabase(
            messages = listOf(message(1L, 100L, "To be or not to be")),
            users = listOf(user(100L, "William Shakespeare", "https://fake.com/avatar100"))
        )

        val viewModel = MessagesViewModel(messagesDao, TestSchedulerProvider())
        val adapterItemObserver = viewModel.adapterItems.testObserver()

        assertThat(adapterItemObserver.observedValues).containsExactly(
            AdapterItems(listOf(
                OtherMessage(1L, "William Shakespeare", "https://fake.com/avatar100", "To be or not to be")
            ), hasMore = false)
        )
    }

    @Test fun emits_other_message_with_attachment() {
        givenDatabase(
            messages = listOf(message(1L, 100L, "To be or not to be")),
            attachments = listOf(attachment(1000L, 1L, "hamlet.jpg", "https://fake.com/hamlet")),
            users = listOf(user(100L, "William Shakespeare", "https://fake.com/avatar100"))
        )

        val viewModel = MessagesViewModel(messagesDao, TestSchedulerProvider())
        val adapterItemObserver = viewModel.adapterItems.testObserver()

        assertThat(adapterItemObserver.observedValues).containsExactly(
            AdapterItems(listOf(
                OtherMessage(1L, "William Shakespeare", "https://fake.com/avatar100", "To be or not to be"),
                OtherAttachment(1000L, "1000", "hamlet.jpg", "https://fake.com/hamlet")
            ), hasMore = false)
        )
    }

    @Test fun when_my_message_is_clicked_emits_show_delete_message_dialog() {
        givenDatabase(
            messages = listOf(message(1L, me().id, "To be or not to be")),
            users = listOf(me())
        )

        val viewModel = MessagesViewModel(messagesDao, TestSchedulerProvider())
        val observer = viewModel.showDeleteMessageDialog.testObserver()

        viewModel.onMyMessageLongClicked(MyMessage(1L, "To be or not to be"))

        assertThat(observer.observedValues).containsExactly(
            ShowDeleteMessageDialogEvent(1L)
        )
    }

    @Test fun when_other_message_is_clicked_emits_show_delete_message_dialog() {
        givenDatabase(
            messages = listOf(message(1L, 100L, "To be or not to be")),
            users = listOf(user(100L, "William Shakespeare", "https://fake.com/avatar100"))
        )

        val viewModel = MessagesViewModel(messagesDao, TestSchedulerProvider())
        val observer = viewModel.showDeleteMessageDialog.testObserver()

        viewModel.onOtherMessageLongClicked(OtherMessage(1L, "William Shakespeare", "https://fake.com/avatar100", "To be or not to be"))

        assertThat(observer.observedValues).containsExactly(
            ShowDeleteMessageDialogEvent(1L)
        )
    }

    @Test fun deletes_message() {
        givenDatabase(
            messages = listOf(message(1L, me().id, "To be or not to be")),
            users = listOf(me())
        )

        val viewModel = MessagesViewModel(messagesDao, TestSchedulerProvider())
        val adapterItemObserver = viewModel.adapterItems.testObserver()

        viewModel.deleteMessage(1L)

        assertThat(adapterItemObserver.observedValues[1]).isEqualTo(
            AdapterItems(listOf(), hasMore = false)
        )
    }

    @Test fun when_my_attachment_is_clicked_emits_show_delete_message_dialog() {
        givenDatabase(
            messages = listOf(message(1L, me().id, "To be or not to be")),
            attachments = listOf(attachment(1000L, 1L, "hamlet.jpg", "https://fake.com/hamlet")),
            users = listOf(me())
        )

        val viewModel = MessagesViewModel(messagesDao, TestSchedulerProvider())
        val observer = viewModel.showDeleteAttachmentDialog.testObserver()

        viewModel.onMyAttachmentLongClicked(MyAttachment(1000L, "1000", "hamlet.jpg", "https://fake.com/hamlet"))

        assertThat(observer.observedValues).containsExactly(
            ShowDeleteAttachmentDialogEvent(1000L)
        )
    }

    @Test fun when_other_attachment_is_clicked_emits_show_delete_message_dialog() {
        givenDatabase(
            messages = listOf(message(1L, 100L, "To be or not to be")),
            attachments = listOf(attachment(1000L, 1L, "hamlet.jpg", "https://fake.com/hamlet")),
            users = listOf(user(100L, "William Shakespeare", "https://fake.com/avatar100"))
        )

        val viewModel = MessagesViewModel(messagesDao, TestSchedulerProvider())
        val observer = viewModel.showDeleteAttachmentDialog.testObserver()

        viewModel.onOtherAttachmentLongClicked(OtherAttachment(1000L, "1000", "hamlet.jpg", "https://fake.com/hamlet"))

        assertThat(observer.observedValues).containsExactly(
            ShowDeleteAttachmentDialogEvent(1000L)
        )
    }

    @Test fun deletes_attachment() {
        givenDatabase(
            messages = listOf(message(1L, me().id, "To be or not to be")),
            attachments = listOf(attachment(1000L, 1L, "hamlet.jpg", "https://fake.com/hamlet")),
            users = listOf(me())
        )

        val viewModel = MessagesViewModel(messagesDao, TestSchedulerProvider())
        val adapterItemObserver = viewModel.adapterItems.testObserver()

        viewModel.deleteAttachment(1000L)

        assertThat(adapterItemObserver.observedValues[1]).isEqualTo(
            AdapterItems(listOf(
                MyMessage(1L, "To be or not to be")
            ), hasMore = false)
        )
    }

    @Test fun initially_loads_up_to_page_size_messages() {
        givenDatabase(
            messages = listOf(
                message(1L, me().id, "To be or not to be"),
                message(2L, me().id, "That is the question"),
                message(3L, me().id, "Whether 'tis nobler")),
            users = listOf(me())
        )

        val viewModel = MessagesViewModel(messagesDao, TestSchedulerProvider(), pageSize = 2)
        val adapterItemObserver = viewModel.adapterItems.testObserver()

        assertThat(adapterItemObserver.observedValues).containsExactly(
            AdapterItems(listOf(
                MyMessage(1L, "To be or not to be"),
                MyMessage(2L, "That is the question")
            ), hasMore = true)
        )
    }

    @Test fun loads_next_page() {
        givenDatabase(
            messages = listOf(
                message(1L, me().id, "To be or not to be"),
                message(2L, me().id, "That is the question"),
                message(3L, me().id, "Whether 'tis nobler"),
                message(4L, me().id, "In the mind to suffer"),
                message(5L, me().id, "The slings and arrows")),
            users = listOf(me())
        )

        val viewModel = MessagesViewModel(messagesDao, TestSchedulerProvider(), pageSize = 2)
        val adapterItemObserver = viewModel.adapterItems.testObserver()

        viewModel.loadNextPage()

        assertThat(adapterItemObserver.observedValues).containsExactly(
            AdapterItems(
                listOf(
                    MyMessage(1L, "To be or not to be"),
                    MyMessage(2L, "That is the question")
                ), hasMore = true
            ),
            AdapterItems(
                listOf(
                    MyMessage(1L, "To be or not to be"),
                    MyMessage(2L, "That is the question"),
                    MyMessage(3L, "Whether 'tis nobler"),
                    MyMessage(4L, "In the mind to suffer")
                ), hasMore = true
            )
        )
    }


    private fun givenDatabase(
        messages: List<MessageEntity> = listOf(),
        attachments: List<AttachmentEntity> = listOf(),
        users: List<UserEntity> = listOf()
    ) {
        messagesDao.insertAll(messages, attachments, users)
    }

    private fun message(id: Long, userId: Long, content: String): MessageEntity {
        return MessageEntity(
            id = id,
            userId = userId,
            content = content
        )
    }

    private fun attachment(id: Long, messageId: Long, title: String, url: String): AttachmentEntity {
        return AttachmentEntity(
            id = id,
            uuid = id.toString(),
            messageId = messageId,
            title = title,
            url = url,
            thumbnailUrl = "https://fake.com/thumbnailUrl"
        )
    }

    private fun user(id: Long, name: String, avatar: String): UserEntity {
        return UserEntity(
            id = id,
            name = name,
            avatarId = avatar
        )
    }

    private fun me() = user(1L, "John Doe", avatar = "https://fake.com/avatarMe")
}