package jedyny.android.doisttestproject.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import jedyny.android.doisttestproject.SchedulerProvider
import jedyny.android.doisttestproject.data.MY_AUTHOR_ID
import jedyny.android.doisttestproject.data.MessageWithAuthorAndAttachments
import jedyny.android.doisttestproject.data.MessagesDao
import jedyny.android.doisttestproject.util.Event

class MessagesViewModel(
    private val messagesDao: MessagesDao,
    private val schedulerProvider: SchedulerProvider,
    private val pageSize: Int = PAGE_SIZE
) : ViewModel() {
    companion object {
        const val PAGE_SIZE = 20
    }

    private val _adapterItems = MutableLiveData<AdapterItems>()
    private val _showDeleteMessageDialog = MutableLiveData<ShowDeleteMessageDialogEvent>()
    private val _showDeleteAttachmentDialog = MutableLiveData<ShowDeleteAttachmentDialogEvent>()
    private val subscriptions = CompositeDisposable()

    private var limit = 0

    val adapterItems: LiveData<AdapterItems> = _adapterItems
    val showDeleteMessageDialog: LiveData<ShowDeleteMessageDialogEvent> = _showDeleteMessageDialog
    val showDeleteAttachmentDialog: LiveData<ShowDeleteAttachmentDialogEvent> = _showDeleteAttachmentDialog

    init {
        loadNextPage()
    }

    override fun onCleared() {
        subscriptions.clear()
    }

    fun loadNextPage() {
        subscriptions.clear()

        limit += pageSize

        subscriptions.add(messagesDao.getMessages(limit)
            .subscribeOn(schedulerProvider.computation)
            .observeOn(schedulerProvider.ui)
            .subscribe { _adapterItems.value = AdapterItems(it.toAdapterItems(), it.size == limit) })
    }

    private fun List<MessageWithAuthorAndAttachments>.toAdapterItems(): List<MessageAdapterItem> {
        val adapterItems = mutableListOf<MessageAdapterItem>()

        for (messageWithAuthorAndAttachments in this) {
            messageWithAuthorAndAttachments.run {
                if (author.id == MY_AUTHOR_ID) {
                    adapterItems += MyMessage(message.id, message.content)
                    adapterItems += attachments.map { MyAttachment(it.id, it.uuid, it.title, it.url) }
                } else {
                    adapterItems += OtherMessage(message.id, author.name, author.avatarId, message.content)
                    adapterItems += attachments.map { OtherAttachment(it.id, it.uuid, it.title, it.url) }
                }
            }
        }

        return adapterItems
    }

    fun onMyMessageLongClicked(adapterItem: MyMessage) {
        _showDeleteMessageDialog.value = ShowDeleteMessageDialogEvent(adapterItem.id)
    }

    fun onOtherMessageLongClicked(adapterItem: OtherMessage) {
        _showDeleteMessageDialog.value = ShowDeleteMessageDialogEvent(adapterItem.id)
    }

    fun deleteMessage(messageId: Long) {
        --limit
        subscriptions.add(Completable.fromCallable { messagesDao.deleteMessage(messageId) }
            .subscribeOn(schedulerProvider.computation)
            .observeOn(schedulerProvider.ui)
            .subscribe())

    }

    fun onMyAttachmentLongClicked(adapterItem: MyAttachment) {
        _showDeleteAttachmentDialog.value = ShowDeleteAttachmentDialogEvent(adapterItem.id)
    }

    fun onOtherAttachmentLongClicked(adapterItem: OtherAttachment) {
        _showDeleteAttachmentDialog.value = ShowDeleteAttachmentDialogEvent(adapterItem.id)
    }

    fun deleteAttachment(attachmentId: Long) {
        subscriptions.add(Completable.fromCallable { messagesDao.deleteAttachment(attachmentId) }
            .subscribeOn(schedulerProvider.computation)
            .observeOn(schedulerProvider.ui)
            .subscribe())
    }
}

data class AdapterItems(val adapterItems: List<MessageAdapterItem>, val hasMore: Boolean)

data class ShowDeleteMessageDialogEvent(val messageId: Long): Event()

data class ShowDeleteAttachmentDialogEvent(val attachmentId: Long): Event()