package jedyny.android.doisttestproject.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentActivity
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import jedyny.android.doisttestproject.R
import kotlinx.android.synthetic.main.messages_delete_attachment_dialog.view.*

class DeleteAttachmentDialogFragment : BottomSheetDialogFragment() {
    companion object {
        private const val ARG_ATTACHMENT_ID = "ATTACHMENT_ID"
        private const val TAG = "DELETE_ATTACHMENT"

        fun show(activity: FragmentActivity, attachmentId: Long) {
            DeleteAttachmentDialogFragment().apply {
                arguments = bundleOf(ARG_ATTACHMENT_ID to attachmentId)
            }.show(activity.supportFragmentManager, TAG)
        }
    }

    private lateinit var callbacks: Callbacks

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (host is Callbacks) {
            callbacks = host as Callbacks
        } else {
            throw ClassCastException("Host does not implement Callbacks.")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.messages_delete_attachment_dialog, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val attachmentId = arguments!!.getLong(ARG_ATTACHMENT_ID)

        view.delete_attachment_button.setOnClickListener {
            callbacks.onDeleteAttachmentClicked(attachmentId)
            dismiss()
        }
    }

    interface Callbacks {
        fun onDeleteAttachmentClicked(attachmentId: Long)
    }
}