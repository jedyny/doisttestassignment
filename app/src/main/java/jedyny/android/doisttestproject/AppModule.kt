package jedyny.android.doisttestproject

import android.content.Context
import jedyny.android.doisttestproject.data.DatabasePopulator
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module

val appModule = module {
    single<SchedulerProvider> { StandardSchedulerProvider() }

    single { androidContext().resources }

    single { androidContext().getSharedPreferences("global.pref", Context.MODE_PRIVATE) }

    single {
        DatabasePopulator(
            get(),
            get(),
            get(),
            get(),
            get()
        )
    }
}