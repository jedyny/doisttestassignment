package jedyny.android.doisttestproject.util

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target

class RoundedImageTarget(private val resources : Resources, private val imageView : ImageView) : Target {
    override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
        val drawable = RoundedBitmapDrawableFactory.create(resources, bitmap)
        drawable.isCircular = true

        imageView.setImageDrawable(drawable)
    }

    override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
        // Do nothing
    }

    override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
        // Do nothing
    }
}