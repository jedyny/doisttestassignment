package jedyny.android.doisttestproject.data

import android.content.Context
import android.content.res.Resources
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import com.google.common.truth.Truth.assertThat
import com.squareup.moshi.Moshi
import jedyny.android.doisttestproject.R
import jedyny.android.doisttestproject.TestSchedulerProvider
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

class DatabasePopulatorTest {
    val context = getApplicationContext<Context>()
    val prefs = context.getSharedPreferences("test.pref", Context.MODE_PRIVATE)
    val database = Room.inMemoryDatabaseBuilder(
        ApplicationProvider.getApplicationContext(),
        Database::class.java
    ).build()
    val dao = database.dao()
    val moshi = Moshi.Builder().build()
    val resources = mock(Resources::class.java)

    val databasePopulator = DatabasePopulator(
        prefs,
        dao,
        moshi,
        resources,
        TestSchedulerProvider()
    )

    @Before
    fun markDatabaseAsNotPopulated() {
        prefs.edit().putBoolean("isDatabasePopulated", false).apply()
    }

    @After
    fun closeDatabase() {
        database.close()
    }

    @Test fun inserts_message() {
        givenData(
            """
                {
                    "messages": [
                        {
                          "id": 1,
                          "userId": 100,
                          "content": "To be or not to be"
                        }
                    ],
                    "users": [
                        {
                            "id": 100,
                            "name": "William Shakespeare",
                            "avatarId": "https://fake.com/avatar"
                        }
                    ]
                }
            """
        )

        val expectedUsers = listOf(
            UserEntity(100L, "William Shakespeare", "https://fake.com/avatar")
        )

        val expectedMessages = listOf(
            MessageEntity(1L, 100L, "To be or not to be")
        )

        databasePopulator.populateDatabaseIfNeeded()

        val actual = dao.getMessages(20).blockingFirst()

        assertThat(actual).containsExactly(
            MessageWithAuthorAndAttachments(expectedMessages[0], expectedUsers[0], listOf())
        )
    }

    @Test fun inserts_multiple_messages() {
        givenData(
            """
                {
                    "messages": [
                        {
                          "id": 1,
                          "userId": 100,
                          "content": "To be or not to be"
                        },
                        {
                          "id": 2,
                          "userId": 100,
                          "content": "That is the question"
                        },
                        {
                          "id": 3,
                          "userId": 100,
                          "content": "Whether 'tis nobler"
                        }
                    ],
                    "users": [
                        {
                            "id": 100,
                            "name": "William Shakespeare",
                            "avatarId": "https://fake.com/avatar"
                        }
                    ]
                }
            """
        )

        val expectedUsers = listOf(
            UserEntity(100L, "William Shakespeare", "https://fake.com/avatar")
        )

        val expectedMessages = listOf(
            MessageEntity(1L, 100L, "To be or not to be"),
            MessageEntity(2L, 100L, "That is the question"),
            MessageEntity(3L, 100L, "Whether 'tis nobler")
        )

        databasePopulator.populateDatabaseIfNeeded()

        val actual = dao.getMessages(20).blockingFirst()

        assertThat(actual).containsExactly(
            MessageWithAuthorAndAttachments(expectedMessages[0], expectedUsers[0], listOf()),
            MessageWithAuthorAndAttachments(expectedMessages[1], expectedUsers[0], listOf()),
            MessageWithAuthorAndAttachments(expectedMessages[2], expectedUsers[0], listOf())
        )
    }

    @Test fun inserts_multiple_messages_multiple_authors() {
        givenData(
            """
                {
                    "messages": [
                        {
                          "id": 1,
                          "userId": 100,
                          "content": "To be or not to be"
                        },
                        {
                          "id": 2,
                          "userId": 200,
                          "content": "All happy families are alike"
                        }
                    ],
                    "users": [
                        {
                            "id": 100,
                            "name": "William Shakespeare",
                            "avatarId": "https://fake.com/avatar100"
                        },
                        {
                            "id": 200,
                            "name": "Leo Tolstoy",
                            "avatarId": "https://fake.com/avatar200"
                        }
                    ]
                }
            """
        )

        val expectedUsers = listOf(
            UserEntity(100L, "William Shakespeare", "https://fake.com/avatar100"),
            UserEntity(200L, "Leo Tolstoy", "https://fake.com/avatar200")

        )

        val expectedMessages = listOf(
            MessageEntity(1L, 100L, "To be or not to be"),
            MessageEntity(2L, 200L, "All happy families are alike")
        )

        databasePopulator.populateDatabaseIfNeeded()

        val actual = dao.getMessages(20).blockingFirst()

        assertThat(actual).containsExactly(
            MessageWithAuthorAndAttachments(expectedMessages[0], expectedUsers[0], listOf()),
            MessageWithAuthorAndAttachments(expectedMessages[1], expectedUsers[1], listOf())
        )
    }

    @Test fun inserts_message_with_attachment() {
        givenData(
            """
                {
                    "messages": [
                        {
                            "id": 1,
                            "userId": 100,
                            "content": "To be or not to be",
                            "attachments": [
                                {
                                  "id": "1000",
                                  "title": "hamlet.jpg",
                                  "url": "https://fake.com/url",
                                  "thumbnailUrl": "https://fake.com/thumbnailUrl"
                                }
                            ]
                        }
                    ],
                    "users": [
                        {
                            "id": 100,
                            "name": "William Shakespeare",
                            "avatarId": "https://fake.com/avatar"
                        }
                    ]
                }
            """
        )

        val expectedUsers = listOf(
            UserEntity(100L, "William Shakespeare", "https://fake.com/avatar")
        )

        val expectedMessages = listOf(
            MessageEntity(1L, 100L, "To be or not to be")
        )

        val expectedAttachments = listOf(
            AttachmentEntity(1L, "1000", 1L, "hamlet.jpg", "https://fake.com/url", "https://fake.com/thumbnailUrl")
        )

        databasePopulator.populateDatabaseIfNeeded()

        val actual = dao.getMessages(20).blockingFirst()

        assertThat(actual).containsExactly(
            MessageWithAuthorAndAttachments(expectedMessages[0], expectedUsers[0], expectedAttachments)
        )
    }

    @Test fun inserts_message_with_multiple_attachments() {
        givenData(
            """
                {
                    "messages": [
                        {
                            "id": 1,
                            "userId": 100,
                            "content": "To be or not to be",
                            "attachments": [
                                {
                                  "id": "1000",
                                  "title": "hamlet.jpg",
                                  "url": "https://fake.com/url1000",
                                  "thumbnailUrl": "https://fake.com/thumbnailUrl1000"
                                },
                                {
                                  "id": "2000",
                                  "title": "macbeth.jpg",
                                  "url": "https://fake.com/url2000",
                                  "thumbnailUrl": "https://fake.com/thumbnailUrl2000"
                                },
                                {
                                  "id": "3000",
                                  "title": "othello.jpg",
                                  "url": "https://fake.com/url3000",
                                  "thumbnailUrl": "https://fake.com/thumbnailUrl3000"
                                }
                            ]
                        }
                    ],
                    "users": [
                        {
                            "id": 100,
                            "name": "William Shakespeare",
                            "avatarId": "https://fake.com/avatar"
                        }
                    ]
                }
            """
        )

        val expectedUsers = listOf(
            UserEntity(100L, "William Shakespeare", "https://fake.com/avatar")
        )

        val expectedMessages = listOf(
            MessageEntity(1L, 100L, "To be or not to be")
        )

        val expectedAttachments = listOf(
            AttachmentEntity(1L, "1000", 1L, "hamlet.jpg", "https://fake.com/url1000", "https://fake.com/thumbnailUrl1000"),
            AttachmentEntity(2L, "2000", 1L, "macbeth.jpg", "https://fake.com/url2000", "https://fake.com/thumbnailUrl2000"),
            AttachmentEntity(3L, "3000", 1L, "othello.jpg", "https://fake.com/url3000", "https://fake.com/thumbnailUrl3000")
        )

        databasePopulator.populateDatabaseIfNeeded()

        val actual = dao.getMessages(20).blockingFirst()

        assertThat(actual).containsExactly(
            MessageWithAuthorAndAttachments(expectedMessages[0], expectedUsers[0], expectedAttachments)
        )
    }

    @Test fun inserts_multiple_messages_multiple_attachments() {
        givenData(
            """
                {
                    "messages": [
                        {
                            "id": 1,
                            "userId": 100,
                            "content": "To be or not to be",
                            "attachments": [
                                {
                                  "id": "1000",
                                  "title": "hamlet.jpg",
                                  "url": "https://fake.com/url1000",
                                  "thumbnailUrl": "https://fake.com/thumbnailUrl1000"
                                }
                            ]
                        },
                        {
                            "id": 2,
                            "userId": 100,
                            "content": "That is the question",
                            "attachments": [
                                {
                                  "id": "2000",
                                  "title": "macbeth.jpg",
                                  "url": "https://fake.com/url2000",
                                  "thumbnailUrl": "https://fake.com/thumbnailUrl2000"
                                }
                            ]
                        }
                    ],
                    "users": [
                        {
                            "id": 100,
                            "name": "William Shakespeare",
                            "avatarId": "https://fake.com/avatar"
                        }
                    ]
                }
            """
        )

        val expectedUsers = listOf(
            UserEntity(100L, "William Shakespeare", "https://fake.com/avatar")
        )

        val expectedMessages = listOf(
            MessageEntity(1L, 100L, "To be or not to be"),
            MessageEntity(2L, 100L, "That is the question")
        )

        val expectedAttachments = listOf(
            AttachmentEntity(1L, "1000", 1L, "hamlet.jpg", "https://fake.com/url1000", "https://fake.com/thumbnailUrl1000"),
            AttachmentEntity(2L, "2000", 2L, "macbeth.jpg", "https://fake.com/url2000", "https://fake.com/thumbnailUrl2000")
        )

        databasePopulator.populateDatabaseIfNeeded()

        val actual = dao.getMessages(20).blockingFirst()

        assertThat(actual).containsExactly(
            MessageWithAuthorAndAttachments(expectedMessages[0], expectedUsers[0], listOf(expectedAttachments[0])),
            MessageWithAuthorAndAttachments(expectedMessages[1], expectedUsers[0], listOf(expectedAttachments[1]))
        )
    }

    @Test fun does_not_populate_database_if_already_populated() {
        givenData(
            """
                {
                    "messages": [
                        {
                          "id": 1,
                          "userId": 100,
                          "content": "To be or not to be"
                        }
                    ],
                    "users": [
                        {
                            "id": 100,
                            "name": "William Shakespeare",
                            "avatarId": "https://fake.com/avatar"
                        }
                    ]
                }
            """
        )

        prefs.edit().putBoolean("isDatabasePopulated", true).apply()

        databasePopulator.populateDatabaseIfNeeded()

        val actual = dao.getMessages(20).blockingFirst()

        assertThat(actual).isEmpty()
    }

    private fun givenData(data: String) {
        `when`(resources.openRawResource(R.raw.data)).thenReturn(data.trimIndent().byteInputStream())
    }
}