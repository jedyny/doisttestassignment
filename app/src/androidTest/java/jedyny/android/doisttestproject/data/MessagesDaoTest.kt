package jedyny.android.doisttestproject.data

import androidx.room.Room
import androidx.sqlite.db.SimpleSQLiteQuery
import androidx.test.core.app.ApplicationProvider
import com.google.common.truth.Truth.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test

class MessagesDaoTest {
    lateinit var database: Database
    lateinit var messagesDao: MessagesDao

    @Before
    fun initDatabase() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            Database::class.java
        ).build()

        messagesDao = database.dao()
    }

    @After
    fun closeDatabase() {
        database.close()
    }

    /*
     * Since inserts use only the @Insert annotation and we trust that Room works, we also don't
     * test them here.
     */

    @Test fun reads_message() {
        val author = user(100L, "William Shakespeare")

        val message = message(1L, 100L, "To be, or not to be")

        messagesDao.insertAll(listOf(message), listOf(), listOf(author))

        val actual = messagesDao.getMessages(20).blockingFirst()

        assertThat(actual).containsExactly(
            MessageWithAuthorAndAttachments(message, author, listOf())
        )
    }

    @Test fun reads_multiple_messages() {
        val author = user(100L, "William Shakespeare")

        val messages = listOf(
            message(1L, 100L, "To be, or not to be"),
            message(2L, 100L, "That is the question"),
            message(3L, 100L, "Whether 'tis nobler")
        )

        messagesDao.insertAll(messages, listOf(), listOf(author))

        val actual = messagesDao.getMessages(20).blockingFirst()

        assertThat(actual).containsExactly(
            MessageWithAuthorAndAttachments(messages[0], author, listOf()),
            MessageWithAuthorAndAttachments(messages[1], author, listOf()),
            MessageWithAuthorAndAttachments(messages[2], author, listOf())
        )
    }

    @Test fun reads_messages_from_multiple_users() {
        val authors = listOf(
            user(100L, "William Shakespeare"),
            user(200L, "Leo Tolstoy")
        )

        val messages = listOf(
            message(1L, 100L, "To be, or not to be"),
            message(2L, 200L, "All happy families are alike")
        )

        messagesDao.insertAll(messages, listOf(), authors)

        val actual = messagesDao.getMessages(20).blockingFirst()

        assertThat(actual).containsExactly(
            MessageWithAuthorAndAttachments(messages[0], authors[0], listOf()),
            MessageWithAuthorAndAttachments(messages[1], authors[1], listOf())
        )
    }

    @Test fun reads_message_with_attachment() {
        val author = user(100L, "William Shakespeare")

        val message = message(1L, 100L, "To be, or not to be")

        val attachment = attachment(1000L, 1L, "hamlet.jpg")

        messagesDao.insertAll(listOf(message), listOf(attachment), listOf(author))

        val actual = messagesDao.getMessages(20).blockingFirst()

        assertThat(actual).containsExactly(
            MessageWithAuthorAndAttachments(message, author, listOf(attachment))
        )
    }

    @Test fun reads_message_with_multiple_attachments() {
        val author = user(100L, "William Shakespeare")

        val message = message(1L, 100L, "To be, or not to be")

        val attachments = listOf(
            attachment(1000L, 1L, "hamlet.jpg"),
            attachment(2000L, 1L, "macbeth.jpg"),
            attachment(3000L, 1L, "othello.jpg")
        )

        messagesDao.insertAll(listOf(message), attachments, listOf(author))

        val actual = messagesDao.getMessages(20).blockingFirst()

        assertThat(actual).containsExactly(
            MessageWithAuthorAndAttachments(message, author, attachments)
        )
    }

    @Test fun reads_multiple_messages_with_attachments() {
        val author = user(100L, "William Shakespeare")

        val messages = listOf(
            message(1L, 100L, "To be, or not to be"),
            message(2L, 100L, "That is the question")
        )

        val attachments = listOf(
            attachment(1000L, 1L, "hamlet.jpg"),
            attachment(2000L, 2L, "macbeth.jpg")
        )

        messagesDao.insertAll(messages, attachments, listOf(author))

        val actual = messagesDao.getMessages(20).blockingFirst()

        assertThat(actual).containsExactly(
            MessageWithAuthorAndAttachments(messages[0], author, listOf(attachments[0])),
            MessageWithAuthorAndAttachments(messages[1], author, listOf(attachments[1]))
        )
    }

    @Test fun reads_up_to_page_size() {
        val author = user(100L, "William Shakespeare")

        val messages = listOf(
            message(1L, 100L, "To be, or not to be"),
            message(2L, 100L, "That is the question"),
            message(3L, 100L, "Whether 'tis nobler")
        )

        messagesDao.insertAll(messages, listOf(), listOf(author))

        val actual = messagesDao.getMessages(2).blockingFirst()

        assertThat(actual).containsExactly(
            MessageWithAuthorAndAttachments(messages[0], author, listOf()),
            MessageWithAuthorAndAttachments(messages[1], author, listOf())
        )
    }

    @Test fun deletes_message_without_attachments() {
        val author = user(100L, "William Shakespeare")

        val message = message(1L, 100L, "To be, or not to be")

        messagesDao.insertAll(listOf(message), listOf(), listOf(author))
        messagesDao.deleteMessage(1L)

        assertThat(messagesDao.getMessages(20).blockingFirst()).isEmpty()
    }

    @Test fun deletes_only_specified_message() {
        val author = user(100L, "William Shakespeare")

        val messages = listOf(
            message(1L, 100L, "To be, or not to be"),
            message(2L, 100L, "That is the question"),
            message(3L, 100L, "Whether 'tis nobler")
        )

        messagesDao.insertAll(messages, listOf(), listOf(author))
        messagesDao.deleteMessage(2L)

        val actual = messagesDao.getMessages(20).blockingFirst()

        assertThat(actual).containsExactly(
            MessageWithAuthorAndAttachments(messages[0], author, listOf()),
            MessageWithAuthorAndAttachments(messages[2], author, listOf())
        )
    }

    @Test fun deletes_attachment() {
        val author = user(100L, "William Shakespeare")

        val message = message(1L, 100L, "To be, or not to be")

        val attachment = attachment(1000L, 1L, "hamlet.jpg")

        messagesDao.insertAll(listOf(message), listOf(attachment), listOf(author))
        messagesDao.deleteAttachment(1000L)

        val actual = messagesDao.getMessages(20).blockingFirst()

        assertThat(actual).containsExactly(
            MessageWithAuthorAndAttachments(message, author, listOf())
        )
    }

    @Test fun deletes_only_specified_attachment() {
        val author = user(100L, "William Shakespeare")

        val message = message(1L, 100L, "To be, or not to be")

        val attachments = listOf(
            attachment(1000L, 1L, "hamlet.jpg"),
            attachment(2000L, 1L, "macbeth.jpg"),
            attachment(3000L, 1L, "othello.jpg")
        )

        messagesDao.insertAll(listOf(message), attachments, listOf(author))
        messagesDao.deleteAttachment(2000L)

        val actual = messagesDao.getMessages(20).blockingFirst()

        assertThat(actual).containsExactly(
            MessageWithAuthorAndAttachments(message, author, listOf(attachments[0], attachments[2]))
        )
    }

    /*
     * The tests below use a raw cursor to avoid adding a test-only method to MessagesDao
     */

    @Test fun cascade_deletes_attachments_of_message() {
        val author = user(100L, "William Shakespeare")

        val message = message(1L, 100L, "To be, or not to be")

        val attachments = listOf(
            attachment(1000L, 1L, "hamlet.jpg"),
            attachment(2000L, 1L, "macbeth.jpg"),
            attachment(3000L, 1L, "othello.jpg")
        )

        messagesDao.insertAll(listOf(message), attachments, listOf(author))
        messagesDao.deleteMessage(1L)

        val cursor = database.query(SimpleSQLiteQuery("SELECT * FROM attachment"))

        assertThat(cursor.count).isEqualTo(0)
    }

    @Test fun cascade_deletes_only_attachments_of_message() {
        val author = user(100L, "William Shakespeare")

        val messages = listOf(
            message(1L, 100L, "To be, or not to be"),
            message(2L, 100L, "That is the question")
        )

        val attachments = listOf(
            attachment(1000L, 1L, "hamlet.jpg"),
            attachment(2000L, 2L, "macbeth.jpg")
        )

        messagesDao.insertAll(messages, attachments, listOf(author))
        messagesDao.deleteMessage(1L)

        val cursor = database.query(SimpleSQLiteQuery("SELECT id FROM attachment"))
        cursor.moveToFirst()

        assertThat(cursor.count).isEqualTo(1)
        assertThat(cursor.getLong(0)).isEqualTo(2000L)
    }

    private fun message(id: Long, userId: Long, content: String): MessageEntity {
        return MessageEntity(
            id = id,
            userId = userId,
            content = content
        )
    }

    private fun attachment(id: Long, messageId: Long, title: String): AttachmentEntity {
        return AttachmentEntity(
            id = id,
            uuid = id.toString(),
            messageId = messageId,
            title = title,
            url = "https://fake.com/url",
            thumbnailUrl = "https://fake.com/thumbnailUrl"
        )
    }

    private fun user(id: Long, name: String): UserEntity {
        return UserEntity(
            id = id,
            name = name,
            avatarId = "https://fake.com/avatar"
        )
    }
}