package jedyny.android.doisttestproject

import android.app.Application
import jedyny.android.doisttestproject.data.DatabasePopulator
import jedyny.android.doisttestproject.data.databaseModule
import jedyny.android.doisttestproject.ui.uiModule
import org.koin.android.ext.android.inject
import org.koin.android.ext.android.startKoin

class App : Application() {
    private val databasePopulator : DatabasePopulator by inject()

    override fun onCreate() {
        super.onCreate()

        startKoin(this, listOf(appModule, databaseModule, uiModule))

        databasePopulator.populateDatabaseIfNeeded()
    }
}