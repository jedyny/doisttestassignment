package jedyny.android.doisttestproject.ui

sealed class MessageAdapterItem {
    abstract val id: Long
}

data class MyMessage(override val id: Long, val content: String) : MessageAdapterItem()

data class OtherMessage(
        override val id: Long,
        val authorName: String,
        val authorAvatarUrl: String,
        val content: String) : MessageAdapterItem()

data class MyAttachment(
        override val id: Long,
        val uuid: String,
        val title: String,
        val url: String) : MessageAdapterItem()

data class OtherAttachment(
        override val id: Long,
        val uuid: String,
        val title: String,
        val url: String) : MessageAdapterItem()