package jedyny.android.doisttestproject.data

import androidx.room.Embedded
import androidx.room.Relation

data class MessageWithAuthorAndAttachments(
    @Embedded(prefix = "message_") val message: MessageEntity,
    @Embedded(prefix = "author_") val author: UserEntity,
    @Relation(entity = AttachmentEntity::class, parentColumn = "message_id", entityColumn = "messageId") val attachments: List<AttachmentEntity>
)