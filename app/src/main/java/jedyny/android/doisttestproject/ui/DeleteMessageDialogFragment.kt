package jedyny.android.doisttestproject.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentActivity
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import jedyny.android.doisttestproject.R
import kotlinx.android.synthetic.main.messages_delete_message_dialog.view.*

class DeleteMessageDialogFragment : BottomSheetDialogFragment() {
    companion object {
        private const val ARG_MESSAGE_ID = "MESSAGE_ID"
        private const val TAG = "DELETE_MESSAGE"

        fun show(activity: FragmentActivity, messageId: Long) {
            DeleteMessageDialogFragment().apply {
                arguments = bundleOf(ARG_MESSAGE_ID to messageId)
            }.show(activity.supportFragmentManager, TAG)
        }
    }

    private lateinit var callbacks: Callbacks

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (host is Callbacks) {
            callbacks = host as Callbacks
        } else {
            throw ClassCastException("Host does not implement Callbacks.")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View  = inflater.inflate(R.layout.messages_delete_message_dialog, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val messageId = arguments!!.getLong(DeleteMessageDialogFragment.ARG_MESSAGE_ID)

        view.delete_message_button.setOnClickListener {
            callbacks.onDeleteMessageClicked(messageId)
            dismiss()
        }
    }

    interface Callbacks {
        fun onDeleteMessageClicked(messageId: Long)
    }
}